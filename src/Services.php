<?php

namespace RoyalMailPriceCalculator;

use RoyalMailPriceCalculator\Services\FirstClassService;
use RoyalMailPriceCalculator\Services\GuaranteedByNineAmService;
use RoyalMailPriceCalculator\Services\GuaranteedByNineAmWithSaturdayService;
use RoyalMailPriceCalculator\Services\GuaranteedByOnePmService;
use RoyalMailPriceCalculator\Services\GuaranteedByOnePmWithSaturdayService;
use RoyalMailPriceCalculator\Services\InternationalEconomy;
use RoyalMailPriceCalculator\Services\InternationalSigned;
use RoyalMailPriceCalculator\Services\InternationalStandard;
use RoyalMailPriceCalculator\Services\InternationalTracked;
use RoyalMailPriceCalculator\Services\InternationalTrackedAndSigned;
use RoyalMailPriceCalculator\Services\SecondClassService;
use RoyalMailPriceCalculator\Services\Service;
use RoyalMailPriceCalculator\Services\SignedForFirstClassService;
use RoyalMailPriceCalculator\Services\SignedForSecondClassService;
use RoyalMailPriceCalculator\Services\Tracked24;
use RoyalMailPriceCalculator\Services\Tracked24WithSignature;
use RoyalMailPriceCalculator\Services\Tracked48;
use RoyalMailPriceCalculator\Services\Tracked48WithSignature;

/**
 * Can provide services.
 */
class Services
{

    /**
     * @return array<string, Service>
     */
    public function getDomesticServices() {
        return [
            'STL1' => new FirstClassService(),
            'STL2' => new SecondClassService(),
            'STL1S' => new SignedForFirstClassService(),
            'STL2S' => new SignedForSecondClassService(),
            'SD9' => new GuaranteedByNineAmService(),
            'SD9S' => new GuaranteedByNineAmWithSaturdayService(),
            'SD1' => new GuaranteedByOnePmService(),
            'SD1S' => new GuaranteedByOnePmWithSaturdayService(),
            'T24' => new Tracked24(),
            'T24S' => new Tracked24WithSignature(),
            'T48' => new Tracked48(),
            'T48S' => new Tracked48WithSignature(),
        ];
    }

    /**
     * @return array<string, Service>
     */
    public function getInternationalServices() {
        return [
            'OLA' => new InternationalStandard(),
            'OLS' => new InternationalEconomy(),
            'OSA' => new InternationalSigned(),
            'OTA' => new InternationalTracked(),
            'OTC' => new InternationalTrackedAndSigned(),
        ];
    }

}
