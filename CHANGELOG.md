## [1.6.5] - 2025-02-24
### Fixed
- Germany moved to Europe Zone 1 (from Zone 3)

## [1.6.4] - 2025-02-20
### Fixed
- US Standard prices

## [1.6.3] - 2024-11-13
### Fixed
- Pricing updated

## [1.6.2] - 2024-10-24
### Fixed
- Packaging type for Tracked 24, Tracked 48, Tracked 24 with Signature, Tracked 48 with Signature services

## [1.6.1] - 2024-04-18
### Fixed
- filenames

## [1.6.0] - 2024-04-18
### Added
- Services: Tracked 24, Tracked 48, Tracked 24 with Signature, Tracked 48 with Signature
### Fixed
- Pricing updated

## [1.5.6] - 2023-09-26
### Fixed
- Pricing updated

## [1.5.5] - 2023-06-29
### Fixed
- JsonSerializable return type

## [1.5.4] - 2023-06-20
### Updated
- YAML library

## [1.5.3] - 2023-04-24
### Fixed
- Economy pricing updated

## [1.5.2] - 2023-04-24
### Fixed
- Pricing updates

## [1.5.1] - 2022-07-04
### Fixed
- Tax rate for 1pm service

## [1.5.0] - 2022-07-04
### Added
- Tax information in prices lists
### Removed
- Old prices lists

## [1.4.2] - 2022-07-04
### Fixed
- Calculator

## [1.4.0] - 2022-06-30
### Added
- 2022 prices

## [1.3.1] - 2022-06-24
### Added
- services
### Fixed
- GuaranteedByNineAmService
- GuaranteedByNineAmWithSaturdayService

## [1.3.0] - 2022-06-24
### Added
- initial version in Octolize
